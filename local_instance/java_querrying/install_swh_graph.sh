echo "Installing dsi-utils"
mvn install:install-file \
   -Dfile=./java/dsiutils/dsiutils-2.7.3-threadSafe.jar \
   -DpomFile=./java/dsiutils/pom.xml

echo "Installing swh-graph"
mvn install:install-file \
   -Dfile=java/swh-graph/swh-graph-2.2.0-threadSafe.jar \
   -DpomFile=java/swh-graph/pom.xml

