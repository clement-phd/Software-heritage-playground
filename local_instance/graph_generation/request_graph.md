# Request test on the graph

[api](https://docs.softwareheritage.org/devel/swh-graph/api.html)

launch the graph : 
```shell
swh graph rpc-serve -g /home/clahoche/Downloads/python_graph/graph
```

## stats

```shell
curl -s -w "\nTime Connect: %{time_connect}s\nTime Start Transfer: %{time_starttransfer}s\nTotal Time: %{time_total}s\n" http://0.0.0.0:5009/graph/stats

{
    "avg_locality": 1072989.016,
    "bits_per_edge": 2.485,
    "bits_per_node": 66.258,
    "compression_ratio": 0.112,
    "indegree_avg": 26.667738084058044,
    "indegree_max": 1949731,
    "indegree_min": 0,
    "num_edges": 1218488928,
    "num_nodes": 45691499,
    "outdegree_avg": 26.667738084058044,
    "outdegree_max": 6384,
    "outdegree_min": 0
}
Time Connect: 0.000096s
Time Start Transfer: 0.004105s
Total Time: 0.004132s
```

## get leaves

```shell
curl -s -w "\nTime Connect: %{time_connect}s\nTime Start Transfer: %{time_starttransfer}s\nTotal Time: %{time_total}s\n" http://0.0.0.0:5009/graph/leaves/swh:1:dir:f4cd8e3c5f2be65a1ec76a315d753a0a5cb37893?max_matching_nodes=10
```