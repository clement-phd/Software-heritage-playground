from typing import Tuple


def split_s3_path(s3_path : str) -> Tuple[str, str]:
    """
    split s3 path into bucket and key
    """
    path_parts=s3_path.replace("s3://","").split("/")
    bucket=path_parts.pop(0)
    key="/".join(path_parts)
    return bucket, key

def human_readable_size(size : float | int, decimal_places=2):
    """Convert a size in bytes to a human-readable format."""
    units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
    for unit in units:
        if size < 1024.0:
            break
        size /= 1024.0
    else:
        # If size is larger than all units, set to the largest unit
        unit = units[-1]
    return f"{size:.{decimal_places}f} {unit}"
