# Local instance
~~The goal is to run a docker local instance of [swh](https://docs.softwareheritage.org/devel/getting-started/using-docker.html). The necessary files can be found [here](https://github.com/SoftwareHeritage/swh-environment/tree/master/docker).~~

The goal is to run a local instance of a [graph](https://docs.softwareheritage.org/devel/swh-graph/quickstart.html) and explore to it.

## Dataset graph
The [small dataset](https://docs.softwareheritage.org/devel/swh-dataset/graph/dataset.html), the [test dataset](https://docs.softwareheritage.org/devel/apidoc/swh.graph.example_dataset.html#module-swh.graph.example_dataset)

try to use the python one : `s3://softwareheritage/graph/2021-03-23-popular-3k-python/compressed/`

## AWS access

[access](https://us-east-2.console.aws.amazon.com/console/home?nc2=h_ct&src=header-signin&region=us-east-2)

Use key (github ssh key) generated in aws console : 
![security key location](../../img/aws_security_location.png)
using aws console :
```shell
aws configure
```

> WARN : use v1 for facilities and only configurings credentials