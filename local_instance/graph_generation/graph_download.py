import argparse
import os
from typing import List, Tuple
import boto3
from tqdm import tqdm
from utils import split_s3_path, human_readable_size

URL = "s3://softwareheritage/graph/2021-03-23-popular-3k-python/compressed/"
OUTPUT_DIR = "/home/clahoche/Downloads/graph/"

def main (s3_bucket : str, output_dir : str, download : bool, files : List[str] | None):
    
    bucket, key = split_s3_path(s3_bucket)
    print("print all elements from s3 bucket : {}, folder : {}".format(bucket, key))

    s3 = boto3.resource('s3')

    my_bucket = s3.Bucket(bucket)

    # Retrieve all objects and their sizes
    object_details = [(obj.key, obj.size) for obj in my_bucket.objects.filter(Prefix=key)]
    object_details.sort(key=lambda obj: obj[1], reverse=True)

    # Determine the length of the longest object key
    max_key_length = max(len(obj[0]) for obj in object_details)

    total_size = sum(obj[1] for obj in object_details)

    # List all objects and their sizes
    for obj in object_details:
        print("Object: {key:<{width}} Size: {size}".format(key=obj[0], width=max_key_length, size=human_readable_size(obj[1])))

    print("Total size: {}".format(human_readable_size(total_size)))

    # Filter out objects that don't match the specified files
    if files is not None:
        object_details = [obj for obj in object_details if os.path.basename(obj[0]) in files ]


    # Filter out already downloaded files
    downloadable_objects : List[Tuple[str, int]] = []
    for obj in object_details:
        base_name = os.path.basename(obj[0])
        local_filename = os.path.join(output_dir, base_name)
        if not os.path.exists(local_filename) or os.path.getsize(local_filename) != obj[1]:
            downloadable_objects.append(obj)
        else:
            print("Skipping {} ({}) (already downloaded)".format(base_name, human_readable_size(obj[1])))

    object_details = downloadable_objects

    total_size = sum(obj[1] for obj in object_details)
    print("Total size to download : {}".format(human_readable_size(total_size)))

    # Download all files with tqdm progress bar
    if download:
        with tqdm(total=total_size, unit='B', unit_scale=True, desc="Downloading", unit_divisor=1024) as pbar:
            for i, obj in enumerate(object_details, 1):
                base_name = os.path.basename(obj[0])
                local_filename = os.path.join(output_dir, base_name)
                os.makedirs(os.path.dirname(local_filename), exist_ok=True)

                # Callback function to update tqdm progress bar
                def progress_callback(bytes_transferred):
                    pbar.update(bytes_transferred)

                pbar.set_description(f"Downloading {base_name} ({human_readable_size(obj[1])}) ({i}/{len(object_details)})")

                my_bucket.download_file(obj[0], local_filename, Callback=progress_callback)



if __name__ == "__main__":
    # Set up argument parser
    parser = argparse.ArgumentParser(description="Download files from an S3 bucket.")
    parser.add_argument("s3_bucket", type=str, help="Name of the S3 bucket")
    parser.add_argument("output_dir", type=str, help="Directory to save downloaded files")
    parser.add_argument("--download", action="store_true", help="Flag to trigger download")
    parser.add_argument("--files", nargs="*", default=None, help="Specific files to download (optional, if not specified all files will be downloaded)")

    # Parse arguments
    args = parser.parse_args()

    main(args.s3_bucket, args.output_dir, args.download, args.files)

