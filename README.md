# Software-heritage-playground

This project will be my playground to play with software heritage. 

## Documentation

- [github](https://github.com/SoftwareHeritage)
- curl [API](https://archive.softwareheritage.org/api/), [index](https://archive.softwareheritage.org/api/1/), [pretty tuto](https://docs.softwareheritage.org/devel/getting-started/api.html)
- tools [here](https://docs.softwareheritage.org/devel/api-reference.html)
- a local [instance](https://docs.softwareheritage.org/devel/getting-started/using-docker.html)
- [architecture](https://docs.softwareheritage.org/devel/architecture/overview.html)

## Nix usage

To use the project, enter the dev console (need Nix) with : `nix develop`

### python packages

> WARN : need to add psycopg2 (needed in the dependances of swh-graph) into nix flake to avoid following error : 
```shell
If you prefer to avoid building psycopg2 from source, please install the PyPI
      'psycopg2-binary' package instead.
```
## Notes

- the metadata seams empty for every git (on the visit endpoint) -> check this